#include "connection.h"
#include <string>
using namespace std;

hwo_connection::hwo_connection(const std::string& host, const std::string& port)  
{
	socket.connectToHost(host.c_str(), atoi(port.c_str()));  
}

hwo_connection::~hwo_connection()
{
	socket.disconnect();
}

jsoncons::json hwo_connection::receive_response(int& error)
{
	string line;
	socket>>line;
	error = !socket.isConnected();
	if (error)
	{
		return jsoncons::json();
	}
	std::stringstream s(line);
	return jsoncons::json::parse(s);
}

void hwo_connection::send_requests(const std::vector<jsoncons::json>& msgs)
{
  jsoncons::output_format format;
  format.escape_all_non_ascii(true);  
  stringstream s;
  for (auto m =msgs.begin();m!=msgs.end();m++) {
    m->to_stream(s, format);
    s << std::endl;
  }
  socket.send(s.str().c_str(),s.str().length());
}
