#ifndef HWO_CONNECTION_H
#define HWO_CONNECTION_H

#include <string>
#include <iostream>
#include "ClientSocketStream.h"
#include <jsoncons/json.hpp>

class hwo_connection
{
public:
  hwo_connection(const std::string& host, const std::string& port);
  ~hwo_connection();
  jsoncons::json receive_response(int& error);
  void send_requests(const std::vector<jsoncons::json>& msgs);

private:
	ClientSocketStream socket;
};

#endif