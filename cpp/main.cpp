#include <iostream>
#include <string>
#include "jsoncons/json.hpp"
#include "protocol.h"
#include "connection.h"
#include <thread>
#include <chrono>
#include "game_logic.h"

using namespace hwo_protocol;

void run(hwo_connection& connection, const std::string& name, const std::string& key, const std::string& track = "")
{
	game_logic game;
	std::vector<jsoncons::json> array;
	if (track == "")
		array.push_back(make_join(name, key));
	else
		array.push_back(make_create_race(name, key,track));
	connection.send_requests( array);
	std::chrono::high_resolution_clock::time_point past,now;
	past = std::chrono::high_resolution_clock::now();
	now = std::chrono::high_resolution_clock::now();
	for (;;)
	{
		int error;
		auto response = connection.receive_response(error);
		now = std::chrono::high_resolution_clock::now();
		//std::cout << "server responded in: " << std::chrono::duration_cast<std::chrono::milliseconds>(now - past).count() << "ms\n";
		past = now;
		if (error)
		{
			std::cout << "Connection closed" << std::endl;
			break;
		}
		else 
		{
			auto res = game.react(response);
			connection.send_requests(res);
			//std::cout << "I responded in: " << std::chrono::duration_cast<std::chrono::milliseconds>(now - past).count() << "ms\n";
			past = now;
		}
	}
}

int main(int argc, const char* argv[])
{
	try
	{
		if (argc < 5 || argc > 6)
		{
			std::cerr << "Usage: ./run host port botname botkey [track]" << std::endl;
			return 1;
		}

		const std::string host(argv[1]);
		const std::string port(argv[2]);
		const std::string name(argv[3]);
		const std::string key(argv[4]);
		std::string track;
		if (argc > 5)
		{
			track = argv[5];
			std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << ", track:"<< track<<std::endl;
		}
		else
			std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;

		hwo_connection connection(host, port);
		run(connection, name, key,track);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;		
		std::cin.get();
		return 2;
	}

	return 0;
}
