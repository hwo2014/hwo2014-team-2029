#ifndef _CLIENTSOCKETSTREAM_
#define _CLIENTSOCKETSTREAM_

#include "SocketStream.h"

class ClientSocketStream : public SocketStream
{	
public:
	ClientSocketStream ();
	ClientSocketStream (const char* pServerIP, int pServerPort);
	bool connect (unsigned long pServeraddr, int pServerPort) ;
	~ClientSocketStream();
	bool connect (const char* pServerIP, int pServerPort);
	bool connectToHost(const char* pServerName, int pServerPort);
};

#endif