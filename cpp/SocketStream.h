#ifndef _SOCKETSTREAM_
#define _SOCKETSTREAM_

#include <sstream>
#include <string>
#include <iostream>

#ifdef WIN32
#include <crtdbg.h>
#include <winsock2.h>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#define SOCKET int
#endif

class SocketStream
{
public :
	enum BlockMode
	{
		BM_ALWAYS,
		BM_NEVER,
		BM_NEEDED
	};

	enum ConnectionMode
	{
		CM_Disconnected = 0,
		CM_Receive = 1,
		CM_Send = 2,
		CM_Both = 3,		
	};

protected:
#ifdef WIN32
	static unsigned mInstancesCreated;
	static WSADATA mWSAData;
#endif
	SOCKET mSocket;
	ConnectionMode mConnected;	
	unsigned mReceiveQueue;
	void initialize();
	BlockMode mBlockMode;
public :
	SocketStream (SOCKET pSOCKET);
	SocketStream (const SocketStream &pStream);
	SocketStream ();
	virtual ~SocketStream();
	bool disconnect(ConnectionMode test = CM_Both);
	void closeSocket();
	inline bool isConnected(ConnectionMode test = CM_Both) const{ return (mConnected & test) == test;}
	inline void SetBlockMode(BlockMode pBlockMode) { mBlockMode = pBlockMode;}
	inline BlockMode GetBlockMode() { return mBlockMode;}
	inline SocketStream& operator = (const SocketStream& pStream)
	{
		mSocket = pStream.mSocket;
		mBlockMode = pStream.mBlockMode;
		mConnected = pStream.mConnected;		
		mReceiveQueue = pStream.mReceiveQueue;
		return *this;
	}

	int send(const char* data, unsigned length);	
	int receive(char* data, unsigned length);
	template <class T> SocketStream& operator << (const T& x)
	{
		if (!mConnected)
			return *this;
		std::stringstream stream;
		stream << x;						
		send(stream.str().c_str(), static_cast<unsigned int>(stream.str().length()));
		return *this;
	}	
	
	SocketStream& operator >> (std::string& x);
};

template <> SocketStream& SocketStream::operator <<(const std::string& x);

#endif