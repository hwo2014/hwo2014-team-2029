#include "Piece.h"
#include <limits>
#include <cmath>

#define TRACK_MAX_SPEED 10
#define TRACK_INFINITY_SPEED 15
#define MAX_RELAX 30
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288419716939937510582097494459230781640
#endif

float Curve_Piece::speedRatio = 5.5;
float Curve_Piece::confidence = 20;

float Curve_Piece::constAddition = 1.50;

Curve_Piece::Curve_Piece() : localConfidence(0)
{

}

float magic_func(float v,float a)
{
	float p[20];
p[ 0] =  0.4686652748;
p[ 1] =  0.5922910242;
p[ 2] = -0.0032450695;
p[ 3] =  0.6465859004;
p[ 4] = -0.0000453858;
p[ 5] = -0.1973732199;
p[ 6] =  0.0060463444;
p[ 7] =  2.3196653859;
p[ 8] =  0.2786418920;
p[ 9] =  0.7798858759;
p[10] = -0.1936300369;
p[11] =  0.9837587093;
p[12] = -0.0720026996;
p[13] = -0.2738444458;
p[14] = -0.0692112254;
p[15] =  7.6522755639;
p[16] =  0.0306226741;
p[17] = -2.2408743879;
p[18] =  0.0984650033;
p[19] =  1.2428207770;
#define newTerm p[__COUNTER__]
	float x0 = (newTerm * pow(v,newTerm) + newTerm * pow(v,newTerm) * exp(v * newTerm) * sin(v * newTerm + newTerm) + newTerm);
	float x1 = (newTerm * pow(v,newTerm) + newTerm * pow(v,newTerm) * exp(v * newTerm) * sin(v * newTerm + newTerm) + newTerm);
	float d = tanh(newTerm * a + newTerm) * newTerm + newTerm;
	return  x0 * d + x1 * (1-d);
}

float Curve_Piece::getMaxThrottle(float currentSpeed, float dist)
{
	float maxSpeed = speedRatio * (fabs(radius + dist * (angle>0?1:-1))) / 100;
	float localMaxSpeed = localSpeedRatio * (fabs(radius + dist * (angle>0?1:-1))) / 100-2;
	maxSpeed = (localConfidence * 2 + maxSpeed) / 3;
	maxSpeed = ignorance * TRACK_MAX_SPEED + (1-ignorance) * maxSpeed;
	float throttle;
	if (currentSpeed > maxSpeed)
	{
		throttle = (2 * maxSpeed - currentSpeed) / currentSpeed;
		throttle = pow(throttle,3);
		throttle *= maxSpeed / TRACK_MAX_SPEED;
	}
	else
		throttle = 1;
	return throttle;
}

float Curve_Piece::getMaxSpeed(float dist)
{
	float maxSpeed = magic_func(fabs(radius + dist * (angle>0?1:-1)),fabs(mergedAngle));
	float localMaxSpeed = localSpeedRatio * (fabs(radius + dist * (angle>0?1:-1))) / 100-2;
	if(maxSpeed > TRACK_INFINITY_SPEED)
		maxSpeed = TRACK_INFINITY_SPEED;
	if (localMaxSpeed > TRACK_INFINITY_SPEED)
		localMaxSpeed = TRACK_INFINITY_SPEED;
//	maxSpeed = (localConfidence * 2 + maxSpeed) / 3;
	maxSpeed = ignorance * TRACK_MAX_SPEED + (1-ignorance) * maxSpeed;
	return maxSpeed;
}

float Curve_Piece::getLength(float dist)
{
	return static_cast<float>(M_PI * (fabs(radius + dist * (angle>0?1:-1))) * (fabs(angle) / 180.f));
}

void Curve_Piece::decMaxSpeed()
{
	speedRatio -= (0.02 / (confidence + 1)) * ignorance;
	localSpeedRatio -= (0.05 / (confidence + 1));
	confidence += 0.2 * ignorance;
	localConfidence += 0.8;
}

void Curve_Piece::incMaxSpeed()
{
	speedRatio += (0.02 / (confidence + 1)) * ignorance;
	localSpeedRatio -= (0.05 / (confidence + 1));
	confidence += 0.2 * ignorance;
	localConfidence += 0.8;
}

void Curve_Piece::prepareMaxSpeed()
{
	if (fabs(angle) < MAX_RELAX)
		ignorance = (MAX_RELAX - fabs(angle)) / MAX_RELAX;
	else
		ignorance = 0;
	localSpeedRatio = (radius / 100) * (radius / 100) * speedRatio;
}

float Straight_Piece::getMaxThrottle(float, float)
{
	return 1;
}

float Straight_Piece::getMaxSpeed(float)
{
	return TRACK_INFINITY_SPEED;
}

float Straight_Piece::getLength(float dist)
{
	return length;
}
