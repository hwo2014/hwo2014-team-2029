#include "game_logic.h"
#include "protocol.h"
#ifdef WIN32
#include <fstream>
#endif

#define SMOOTH_FACTOR 6

using namespace hwo_protocol;
using namespace std;

game_logic::game_logic()  
{
	action_map["join"] = &game_logic::on_join;
	action_map["gameStart"] = &game_logic::on_game_start;
	action_map["carPositions"] = &game_logic::on_car_positions;
	action_map["crash"] = &game_logic::on_crash;
	action_map["gameEnd"] = &game_logic::on_game_end;
	action_map["error"] = &game_logic::on_error;
	action_map["yourCar"] = &game_logic::on_your_car;
	action_map["gameInit"] = &game_logic::on_game_init;
	action_map["turboAvailable"] = &game_logic::on_turbo_avail;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (msg.has_member("gameTick") && msg_type == "carPositions")
		printf("%4d ,",msg["gameTick"].as_int());
	if (action_it != action_map.end())
	{
		return (this->*action_it->second)(data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;
		msg_vector res;
		res.push_back(make_ping());
		return res;
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;
	msg_vector res;
	res.push_back(make_ping());
	return res;
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	color = data["color"].as_string();
	std::cout << "I am " << color << std::endl;
	msg_vector res;
	res.push_back(make_ping());
	return res;
}

void game_logic::prepareForRace()
{
	lastPos.first = 0;
	lastPos.second = 0;
	lastAngle = 0;
	turboAvailable = false;
	lastPos = make_pair(0,0);
	turboLength = -1;
	maxSpeeds.clear();
	turboThreshold = 0;
	pieces.clear();
	lanes.clear();
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	const jsoncons::json& track = data["race"]["track"];
	const jsoncons::json& pieces = track["pieces"];
	const jsoncons::json& lanes = track["lanes"];	
	cout << "track = " << track["name"] << "\n";
	prepareForRace();
	for(unsigned i=0;i<pieces.size();i++)
	{
		Base_Piece* piece;
		if (pieces[i].has_member("length"))
		{
			piece = new Straight_Piece;
			static_cast<Straight_Piece*> (piece)->length = pieces[i]["length"].as_double();
			if (pieces[i].has_member("switch"))
				static_cast<Straight_Piece*> (piece)->hasSwitch = pieces[i]["switch"].as_bool();
			else
				static_cast<Straight_Piece*> (piece)->hasSwitch = false;
		}
		if (pieces[i].has_member("angle"))
		{
			piece = new Curve_Piece;
			static_cast<Curve_Piece*> (piece)->angle = pieces[i]["angle"].as_double();
			static_cast<Curve_Piece*> (piece)->radius = pieces[i]["radius"].as_double();
			static_cast<Curve_Piece*> (piece)->prepareMaxSpeed();
		}

		this->pieces.push_back(piece);
	}
	float mergedAngle = 0;
	for(unsigned i=0;i<this->pieces.size();i++)
	{
		Curve_Piece* piece = dynamic_cast<Curve_Piece*> (this->pieces[i]);
		if (piece)
		{
			if (mergedAngle * piece->angle < 0)
				mergedAngle = sqrt(fabs(mergedAngle)) * (mergedAngle > 0?1:-1);
			mergedAngle += piece->angle;
			unsigned j;
			for(j=i+1;j<i+this->pieces.size();j++)
			{
				Curve_Piece* newPiece = dynamic_cast<Curve_Piece*> (this->pieces[j % this->pieces.size()]);
				if (newPiece && newPiece->radius == piece->radius && newPiece->angle * piece->angle > 0)
					mergedAngle += newPiece->angle;
				else
					break;
			}
			for(;i<j;i++)
				static_cast<Curve_Piece*>(this->pieces[i % this->pieces.size()])->mergedAngle = mergedAngle;
			i--;
		}
		else
			if(dynamic_cast<Straight_Piece*> (this->pieces[i]))
				mergedAngle *= 1 / sqrt(1 + dynamic_cast<Straight_Piece*> (this->pieces[i])->length) * cos(dynamic_cast<Straight_Piece*>(this->pieces[i])->length / 140 * M_PI);
	}
	for(unsigned i=0;i<lanes.size();i++)
		this->lanes[lanes[i]["index"].as_int()] = -lanes[i]["distanceFromCenter"].as_double();

	float laneLength = 0;
	for(unsigned i=0;i<this->pieces.size();i++)
		laneLength += this->pieces[i]->getLength(0);
	for(unsigned i=0;i<lanes.size();i++)
	{		
		maxSpeeds.push_back(vector<float>((unsigned)laneLength,0));
		unsigned trackID = 0;
		float trackProgress = 0;
		for(unsigned j=0;j<maxSpeeds[i].size();j++)
		{
			trackProgress++;
			if (trackProgress > this->pieces[trackID]->getLength(0))
			{
				trackProgress -= this->pieces[trackID]->getLength(0);
				trackID++;
			}
			maxSpeeds[i][j] = this->pieces[trackID]->getMaxSpeed(this->lanes[i]);
		}
		
		for(int l=0;l<SMOOTH_FACTOR;l++)
		for(unsigned j=0;j<maxSpeeds[i].size();j++)
		{
			float sum = 0;
			float sumValue = 0;
			float value;
			for(unsigned k=0;k<maxSpeeds[i][j] * maxSpeeds[i][j];k++)
			{
				if(maxSpeeds[i][j] >= maxSpeeds[i][(j+k)%maxSpeeds[i].size()])
				{
					value = maxSpeeds[i][j]*maxSpeeds[i][j] - k; 
					sum += maxSpeeds[i][(j+k) % maxSpeeds[i].size()] * value;
					sumValue += value;
				}
			}
			maxSpeeds[i][j] = sum / sumValue;
		}
	}
#ifdef WIN32
	ofstream speedLog("speed.txt");
	speedLog.setf(ios::fixed);
	speedLog.precision(5);
	
	for(unsigned i=0;i<maxSpeeds[0].size();i++)
	{
		for(unsigned j=0;j<maxSpeeds.size();j++)
		{
			speedLog << maxSpeeds[j][i] << " ";
		}
		speedLog<<endl;
	}
#endif
	for(unsigned i=0;i<maxSpeeds.size();i++)
		for(unsigned j=0;j<maxSpeeds[i].size();j++)
		{
			maxSpeeds[i][j] *= 0.97;
			maxSpeeds[i][j] -= 0.098;
		}
	msg_vector res;
	res.push_back(make_ping());
	turboThreshold = 0;
	for(unsigned i=0;i<maxSpeeds.size();i++)
		for(unsigned j=0;j<maxSpeeds[i].size();j++)
		{
			float tms = maxSpeeds[i][j];
			for(unsigned k=0;k<maxSpeeds[i][j] * 2;k++)
				if(tms > maxSpeeds[i][(j+k)%maxSpeeds[i].size()])
					tms = maxSpeeds[i][(j+k)%maxSpeeds[i].size()];
			if(turboThreshold < tms)
				turboThreshold = tms;
		}
	turboThreshold *= 0.90;
	for(unsigned i=0;i<maxSpeeds.size();i++)
		for(unsigned j=0;j<maxSpeeds[i].size();j++)
		{
			float tms = maxSpeeds[i][j];
			for(unsigned k=0;k<maxSpeeds[i][j] * 2;k++)
				if(tms > maxSpeeds[i][(j+k)%maxSpeeds[i].size()])
					tms = maxSpeeds[i][(j+k)%maxSpeeds[i].size()];
			if(turboThreshold < tms)
				turboSpots.insert(j);
		}

	return res;
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;
	msg_vector res;
	res.push_back(make_ping());
	return res;
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	int infID;
	for(unsigned i=0;i<data.size();i++)
	{
		if (data[i]["id"]["color"] == color)
		{
			infID = i;
			break;
		}
	}	
	const jsoncons::json& inf = data[infID];
	const jsoncons::json& pieceinfo = inf["piecePosition"];
	int pieceIdx = pieceinfo["pieceIndex"].as_int();
	float inPieceDistance = pieceinfo["inPieceDistance"].as_double();
	int lane = pieceinfo["lane"]["startLaneIndex"].as_int();
	float angle = static_cast<float>(inf["angle"].as_double()),omega;
	float speed;
	if (pieceIdx != lastPos.first)
		speed = pieces[lastPos.first]->getLength(lanes[lane]) + inPieceDistance - lastPos.second;
	else
		speed = inPieceDistance - lastPos.second;
	
	msg_vector res;
	lastPos.first = pieceIdx;
	lastPos.second = inPieceDistance;
	float throttle;

	float imgPosition = 0;
	for(unsigned i=0;i<pieceIdx;i++)
		imgPosition += pieces[i]->getLength(0);
	
	imgPosition += inPieceDistance * pieces[pieceIdx]->getLength(0) / pieces[pieceIdx]->getLength(lanes[lane]);

	float maxSpeed = maxSpeeds[lane][imgPosition];
	float tv = maxSpeeds[lane][imgPosition];
	if (turboAvailable && turboSpots.find(imgPosition) != turboSpots.end())
	{
		res.push_back(make_turbo());
		turboAvailable = false;
		printf("turbo\n");
		return res;
	}
	maxSpeed = maxSpeeds[lane][static_cast<int>(imgPosition) % maxSpeeds[lane].size()];
	for(unsigned i=0;i<speed*speed / 3;i++)
		if (maxSpeed > maxSpeeds[lane][static_cast<int>(imgPosition+i) % maxSpeeds[lane].size()])
			maxSpeed = maxSpeeds[lane][static_cast<int>(imgPosition+i) % maxSpeeds[lane].size()];

	omega = angle - lastAngle;
	lastAngle = angle;

	float ratio = 1;
	const float step = 0.05;
	for(float i=1;i<=7;i+=step)
		if (fabs(fabs(angle) + copysign(omega * omega,angle*omega) * i) > 50)
			ratio *= pow(0.97,step);
		else
			if(fabs(fabs(angle) + copysign(omega * omega,angle*omega) * i) < 45)
				ratio *= pow(1.018,step);

	if (ratio < 0.9999)
		for(unsigned j=0;j<lanes.size();j++)
			for(int i=-speed;i<speed*speed;i++)
				if( maxSpeeds[j][static_cast<int>(imgPosition+i + maxSpeeds[j].size())%maxSpeeds[j].size()] <= maxSpeed)
					maxSpeeds[j][static_cast<int>(imgPosition+i+maxSpeeds[j].size())%maxSpeeds[j].size()] *= ratio;
				else
					break;
	if (ratio > 1.0001)
	{
//		ratio = sqrt(ratio);
		for(unsigned j=0;j<maxSpeeds.size();j++)
			for(int i=speed/2;i<2.5 * speed;i++)
				maxSpeeds[j][static_cast<int>(imgPosition-i + maxSpeeds[j].size())%maxSpeeds[j].size()] *= pow(ratio,1 / (1+sqrt(i)));
	}

	maxSpeed *= pow(ratio,0.1);


	if (speed > maxSpeed && maxSpeed < 11.8)
	{
		throttle = (2 * maxSpeed - speed) / speed;
		throttle = pow(throttle,7);
		throttle *= maxSpeed / 10;
	}
//	throttle = 0;
	else
		throttle = 1;

	printf("s = %5.2f, ms = %5.2f => t = %4.2f (cr = %5.2f, ca = %5.2f, a = %+06.2f, t = %4d, tv = %4.1f, tt = %4.1f)->%5.2f\n",speed,maxSpeed, throttle,
		dynamic_cast<Curve_Piece*>(pieces[pieceIdx])?
		dynamic_cast<Curve_Piece*>(pieces[pieceIdx])->radius + lanes[lane] * (dynamic_cast<Curve_Piece*>(pieces[pieceIdx])->angle>0?1:-1):0,
		dynamic_cast<Curve_Piece*>(pieces[pieceIdx])?
		dynamic_cast<Curve_Piece*>(pieces[pieceIdx])->mergedAngle:0,
		inf["angle"].as_double(),
		turboAvailable?turboLength:-1,
		tv,
		turboThreshold,
		maxSpeeds[lane][static_cast<int>(imgPosition - speed * speed + maxSpeeds[lane].size()) % maxSpeeds[lane].size()]
		);	
	if (throttle < 0)
		throttle = 0;		
	res.push_back(make_throttle(throttle));
	return res;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Someone crashed" << std::endl;
	msg_vector res;
	res.push_back(make_ping());
	return res;
}

game_logic::msg_vector game_logic::on_turbo_avail(const jsoncons::json& data)
{
	turboAvailable = true;
	turboLength = data["turboDurationTicks"].as_int();
	msg_vector res;
	res.push_back(make_ping());
	return res;
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Race ended" << std::endl;
	msg_vector res;
	res.push_back(make_ping());
	return res;
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	msg_vector res;
	res.push_back(make_ping());
	return res;
}
