#pragma once

struct Base_Piece
{
	virtual float getMaxThrottle(float currentSpeed, float distFromCenter) = 0;
	virtual float getMaxSpeed(float distFromCenter) = 0;
	virtual float getLength(float distFromCenter) = 0;
};

struct Curve_Piece : public Base_Piece
{
	Curve_Piece();
	virtual float getMaxThrottle(float currentSpeed, float distFromCenter);
	virtual float getMaxSpeed(float distFromCenter);
	virtual float getLength(float distFromCenter);
	virtual void incMaxSpeed();
	virtual void decMaxSpeed();
	void prepareMaxSpeed();

	float angle;
	float mergedAngle;
	float radius;
//private:	
	static float confidence;
	static float speedRatio;
	static float constAddition;
	float localSpeedRatio;
	float localConfidence;
	float ignorance;
};

struct Straight_Piece: public Base_Piece
{
	virtual float getMaxThrottle(float currentSpeed, float distFromCenter);
	virtual float getMaxSpeed(float distFromCenter);
	virtual float getLength(float distFromCenter);
	float length;
	bool hasSwitch;
};
