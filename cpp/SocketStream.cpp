#include "SocketStream.h"
#include <string.h>
#ifndef min
#define min(X,Y) ((X)<(Y)?(X):(Y))
#endif

#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
unsigned SocketStream::mInstancesCreated = 0;
WSADATA SocketStream::mWSAData;
#endif

SocketStream::SocketStream()
{
#ifdef WIN32
	if (mInstancesCreated == 0) 
		WSAStartup(MAKEWORD(2,0), &mWSAData); 
	mInstancesCreated ++; 
#endif
	mBlockMode = BM_NEEDED;
	mReceiveQueue = 0;
	mConnected = CM_Disconnected;
	mSocket = -1;
}

SocketStream::SocketStream(SOCKET pSocket) : mSocket (pSocket)
{
#ifdef WIN32
	if (mInstancesCreated == 0) 
		WSAStartup(MAKEWORD(2,0), &mWSAData); 
	mInstancesCreated ++; 
#endif
	mBlockMode = BM_NEEDED;
	mReceiveQueue = 0;
	if (pSocket >= 0)
		mConnected = CM_Both;
	else
	{
		mConnected = CM_Disconnected;
		mSocket = -1;
	}
}

SocketStream::SocketStream(const SocketStream &pStream)
{
#ifdef WIN32
	mInstancesCreated ++; 
#endif
	mSocket = pStream.mSocket;
	mBlockMode = pStream.mBlockMode;
	mConnected = pStream.mConnected;
	mReceiveQueue = 0;
}

SocketStream::~SocketStream()
{
#ifdef WIN32
	mInstancesCreated --; // Decrease The Number of instances Created from this class
	if (mInstancesCreated == 0) // Check if this class is the last class using Socket
		WSACleanup(); // Clean up dll loaded Files
#endif
}

void SocketStream::initialize()
{
	mConnected = CM_Disconnected;
	mBlockMode = BM_NEEDED;
	mSocket = socket( AF_INET, // Creates the Socket , The Internet Protocol version 4 (IPv4) address family.
		SOCK_STREAM,  // Provides sequenced, reliable, two-way, connection-based byte streams 
		IPPROTO_TCP ); // The Transmission Control Protocol (TCP).	
#ifdef WIN32
	if (mSocket == INVALID_SOCKET) // Invalid Socket
		_ASSERT(mSocket != INVALID_SOCKET && "Socket Creation Failed.");
#endif
}

int SocketStream::send(const char* data, unsigned length)
{	
	if (!isConnected(CM_Send))
		return 0;	
	int temp;
	unsigned bytesToSend = length;	
	unsigned bytesSent = 0;
	while (bytesSent < bytesToSend)
	{
		temp = ::send(mSocket, data + bytesSent, bytesToSend - bytesSent, 0); 
        int error =0;
#ifdef WIN32
		error = WSAGetLastError();
#endif
		if (temp <= 0)
		{
			disconnect(CM_Send);
			return bytesSent;
		}			
		bytesSent += temp;	
		if (mBlockMode != BM_ALWAYS && (mBlockMode != BM_NEEDED))
			break;
				
	}
	return bytesSent;		
}

int SocketStream::receive(char* data, unsigned length)
{
	if (!isConnected(CM_Receive))
		return 0;
	
	int temp;
	unsigned bytesreceived = 0;
	while (bytesreceived < length)
	{
		temp = ::recv(mSocket, data + bytesreceived, length - bytesreceived, 0); 
        int error =0;
#ifdef WIN32
		error = WSAGetLastError();
#endif
		if (temp <= 0)
		{
			disconnect();
			return bytesreceived;
		}
		bytesreceived += temp;
		if (mBlockMode != BM_ALWAYS && (mBlockMode != BM_NEEDED))
			break;
	}
	return bytesreceived;		
}

SocketStream& SocketStream::operator >> (std::string& x)
{
	if (!isConnected(CM_Receive))
		return *this;		
	char c[1];
	x = "";
	while (x.length() == 0 || x[x.length() - 1] != '\n')
	{
		if (receive(c,1) >0)			
			x.append(c,1);			
		if (!isConnected())
			break;
	}		
	return *this;
}

bool SocketStream::disconnect(ConnectionMode cm)
{	
	if (mSocket < 0)
	{
		mConnected = CM_Disconnected;
		return false;
	}

	if ((mConnected & cm) != 0)
	{
		bool result = shutdown(mSocket, (mConnected & cm) - 1) == 0;
		mConnected = static_cast<ConnectionMode>(mConnected & ~cm);
		return result;
	}
	return false;
}

void SocketStream::closeSocket()
{
#ifdef WIN32
	::closesocket(mSocket);
#else
	::close(mSocket);
#endif
	mSocket = -1;
}

template <> SocketStream& SocketStream::operator <<(const std::string& x)
{
	if (!isConnected(CM_Send))
		return *this;
	send(x.c_str(), static_cast<unsigned int>(x.length()));
	return *this;
}