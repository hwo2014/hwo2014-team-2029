#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include "Piece.h"
#include <jsoncons/json.hpp>
#include <set>

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;
  
  game_logic();
  msg_vector react(const jsoncons::json& msg);  
private:
  typedef msg_vector(game_logic::*action_fun)(const jsoncons::json&);
  std::map<std::string, action_fun> action_map;
  std::set<int> turboSpots;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_turbo_avail(const jsoncons::json& data);

  void prepareForRace();
  bool turboAvailable;
  int turboLength;
  float turboThreshold;
  std::pair<int,float> lastPos;
  float lastAngle;
  std::string color;
  std::vector<std::vector<float>> maxSpeeds;
  std::vector<Base_Piece*> pieces;
  std::map<int,float> lanes;
};

#endif
