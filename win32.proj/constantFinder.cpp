#include <cmath>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ctime>

using namespace std;

double randf(double x)
{
	return rand() / static_cast<double>(RAND_MAX) * x;
}

double bd;

double mutation_factor()
{
	return 1 + (tanh(log(bd/2)) + 1)/100;
}

double penalty(double diff)
{
	if (diff > 0)
		return diff * diff * 10;
	else
		return diff * diff;
}

struct animal
{
	double a,b,c,d;
	int age;
	animal()
	{
		a = randf(0.05) + 0.45;
		b = randf(0.05) + 0.56;
		c = randf(1000) + 5600;
		d = randf(0.1) + 1.0025;
		age = 0;
	}
	double f(double v)
	{
		return a * pow(v,b) /** exp(-v/c)*/ + d;
	}
	animal operator +(animal x)
	{
		animal res;
		res.a = (a + x.a) / 2;
		res.b = (b + x.b) / 2;
		res.c = (c + x.c) / 2;
		res.d = (d + x.d) / 2;
		return res;
	}
	animal operator *(animal x)
	{
		animal res;
		res.a = (a + x.a) / 2;
		res.b = (b + x.b) / 2;
		res.c = (c + x.c) / 2;
		res.d = (d + x.d) / 2;
		int r = rand() % 8;
		switch (r)
		{
		case 0: res.a *= mutation_factor(); break;
		case 1: res.b *= mutation_factor(); break;
		case 2: res.c *= mutation_factor(); break;
		case 3: res.d *= mutation_factor(); break;
		case 4: res.a /= mutation_factor(); break;
		case 5: res.b /= mutation_factor(); break;
		case 6: res.c /= mutation_factor(); break;
		case 7: res.d /= mutation_factor(); break;
		default:
			break;
		}
		return res;
	}

	bool operator<(const animal& other) const
	{
		return false;
	}
};

int main()
{
	srand(time(0));
	animal a;
	a.a = 0.4606;
	a.b = 0.5566;
	a.c = 6114.7461;
	a.d = 1.0742;
	vector<pair<double,double>> tests;
	vector<pair<double,animal>> animals;
	tests.push_back(make_pair(220,10.05)); 
	tests.push_back(make_pair(110,7)); 
	tests.push_back(make_pair(60,5.8));
	tests.push_back(make_pair(40,4.5));

	const unsigned generationSize = 200;

	for(unsigned i=0;i<generationSize;i++)
		animals.push_back(make_pair(0,animal()));
	for(unsigned i=0;i<animals.size();i++)
	{
		animals[i].first = 0;
		for(unsigned j=0;j<tests.size();j++)
		{
			double diff = animals[i].second.f(tests[j].first) - tests[j].second;
			animals[i].first += penalty(diff);
		}
	}
	bd = 1000;
	int generation = 0;
	while(true)
	{
		generation ++;
		sort(animals.begin(),animals.end());
		if (generation % 100 == 0)
		{
			printf("generation %d, diff = %10.2f, age = %d and values: \na=%15.10f;\nb=%15.10f;\nc=%15.10f;\n",
				generation,
				animals.front().first,
				animals.front().second.age,
				animals.front().second.a,
				animals.front().second.b,
				//animals.front().second.c,
				animals.front().second.d);
			for(unsigned i=0;i<tests.size();i++)
			{
				printf("%v = %6.2f, f(v) = %6.2f, d = %6.2f\n",
					tests[i].first,
					animals.front().second.f(tests[i].first),
					animals.front().second.f(tests[i].first) - tests[i].second);
			}
			printf("%10.10f\n",mutation_factor());
		}
		bd = animals.front().first;
		/*auto removeList = 
			remove_if(animals.begin(),animals.end(),
			[](const pair<double,animal>& a)->bool{return a.second.age > 100;});
		animals.erase(removeList,animals.end());*/
		if (animals.size() >generationSize)
			animals.resize(generationSize);
		for(unsigned i=0;i<generationSize;i++)
			for(unsigned j=0;j<i;j++)				
					animals.push_back(make_pair(0,animals[i].second + animals[j].second));
		for(unsigned i=0;i<generationSize/2;i++)
			for(unsigned j=0;j<generationSize/2;j++)				
					animals.push_back(make_pair(0,animals[i].second * animals[j].second));
		for(unsigned i=generationSize;i<animals.size();i++)
		{
			animals[i].first = 0;
			for(unsigned j=0;j<tests.size();j++)
			{
				double diff = animals[i].second.f(tests[j].first) - tests[j].second;
				animals[i].first += penalty(diff);
			}
		}
		for(unsigned i=0;i<animals.size();i++)
			animals[i].second.age ++;
	}
	
	cin.get();
}